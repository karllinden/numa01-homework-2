#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  Solve_Intervals.py
#
#  Copyright 2015 Sölve Ryding <solve.ryding.254@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

"""
Created on Wed Dec  9 15:47:01 2015
@author: Sölve Ryding
"""
import numpy as np
import matplotlib.pyplot as plt
import math
class Interval(object):
    """
    An interval with lowerbound as lower endpoint, and upperbound as upper
    endpoint. If no upperbound is provided, the degenerate interval:
    Interval(lowerbound,lowerbound) is returned.

    The boundaries must be real values and the lower endpoint must be
    less than or equal to the upper one. Otherwise errors are raised.

    The lowerbound must be a list of two real values, a list of one real
    value or a real value. The uppervalue must be a real value or omitted.
    """
    def __init__(self, lowerbound, upperbound = None):
        try:
            if upperbound == None and isinstance(lowerbound, list):
                float(lowerbound[0])
                if len(lowerbound) > 1:
                    float(lowerbound[1])
            elif upperbound != None:
                float(upperbound)
                if isinstance(lowerbound,list):
                    float(lowerbound[0])
                else:
                    float(lowerbound)
            else:
                float(lowerbound)
        except ValueError:
            raise ValueError("The bounds contained non-real number(s).")
        if upperbound == None:
            if isinstance(lowerbound, list):
                if len(lowerbound) == 2:
                    self.lowerbound = lowerbound[0]
                    self.upperbound = lowerbound[1]
                elif len(lowerbound) == 1:
                    self.lowerbound = lowerbound[0]
                    self.upperbound = lowerbound[0]
                else:
                    raise ValueError(
                    "Can't convert list of length {length} to an interval."
                    .format(length = len(lowerbound))
                    )
            else:
                self.lowerbound = lowerbound
                self.upperbound = lowerbound
        else:
            if isinstance(lowerbound, list) and len(lowerbound) != 1:
                raise ValueError(
                "Don't provide two values if the first value is a list of",
                "of length greater than one."
                )
            if isinstance(lowerbound, list):
                self.lowerbound = lowerbound[0]
            else:
                self.lowerbound = lowerbound
            self.upperbound = upperbound
        if self.lowerbound>self.upperbound:
            raise ValueError(
            "Lower bound {l} is not less than or equal to upper bound {u}"
            .format(l = self.lowerbound, u = self.upperbound)
            )
        if math.isinf(self.lowerbound) or math.isinf(self.upperbound):
            raise ValueError("Interval may not be of infinite length.")
        if math.isnan(self.lowerbound) or math.isnan(self.upperbound):
            raise ValueError(
                "One or more of the bounds were NaN (Not a number).")

    def __add__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        lower1 = self.lowerbound
        upper1 = self.upperbound
        lower2 = other.lowerbound
        upper2 = other.upperbound
        return Interval(lower1 + lower2, upper1 + upper2)

    #Interval addition commutes
    __radd__ = __add__

    def __iadd__(self,other):
        if not isinstance(other, Interval):
            self += Interval(other)
        else:
            self.lowerbound += other.lowerbound
            self.upperbound += other.upperbound
        return self

    def __sub__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        lower1 = self.lowerbound
        upper1 = self.upperbound
        lower2 = other.lowerbound
        upper2 = other.upperbound
        return Interval(lower1 - upper2, upper1 - lower2)

    def __rsub__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        return other - self

    def __isub__(self,other):
        if not isinstance(other, Interval):
            other = Interval(other)
        self.lowerbound -= other.upperbound
        self.upperbound -= other.lowerbound

    def __mul__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        lower1 = self.lowerbound
        upper1 = self.upperbound
        lower2 = other.lowerbound
        upper2 = other.upperbound
        combinations = [
            lower1 * lower2,
            lower1 * upper2,
            upper1 * lower2,
            upper1 * upper2
            ]
        return Interval(min(combinations), max(combinations))

    #Interval multiplication commutes.
    __rmul__ = __mul__

    def __imul__(self,other):
        if not isinstance(other, Interval):
            other = Interval(other)
        lower1 = self.lowerbound
        upper1 = self.upperbound
        lower2 = other.lowerbound
        upper2 = other.upperbound
        combinations = [
            lower1 * lower2,
            lower1 * upper2,
            upper1 * lower2,
            upper1 * upper2
            ]
        return Interval(min(combinations), max(combinations))

    def __truediv__(self, other):
        if not isinstance(other, Interval):
            other = Interval(other)
        lower1 = self.lowerbound
        upper1 = self.upperbound
        lower2 = other.lowerbound
        upper2 = other.upperbound
        if 0 in other:
            raise ZeroDivisionError("division by zero")
        combinations = [
            lower1 / lower2,
            lower1 / upper2,
            upper1 / lower2,
            upper1 / upper2
            ]
        if float("inf") in combinations or -float("inf") in combinations:
            raise ValueError("Infinite length of interval.")
        return Interval(min(combinations), max(combinations))

    def __rtruediv__(self, other):
        return Interval(other) / self

    def __itruediv__(self,other):
        return self / other

    def __pow__(self, other):
        if not isinstance(other,int) or other <= 0:
            raise TypeError("Currently, intervals can only be taken to",
                "powers of nonnegative integers")
        lower = self.lowerbound
        upper = self.upperbound
        if lower >= 0 or other % 2 == 1:
            newlower = lower ** other
            newupper = upper ** other
        elif upper < 0:
            newlower = upper ** other
            newupper = lower ** other
        else:
            newlower = 0
            newupper = max(lower ** other, upper ** other)
        return Interval(newlower, newupper)

    def __repr__(self):
        return ("[{lower}, {upper}]"
                    .format(lower = self.lowerbound, upper = self.upperbound))

    def __contains__(self, other):
        if other >= self.lowerbound and other <= self.upperbound:
            return True
        else:
            return False

xl = np.linspace(0., 1, 1000)
xu = xl+0.5
temp=[Interval(xl[i], xu[i]) for i in range(len(xl))]
yintervals=[3 * I ** 3 - 2 * I ** 2 + 5 * I - 1 for I in temp]
xplot = xl
yplot1 = [y.lowerbound for y in yintervals]
yplot2 = [y.upperbound for y in yintervals]
plt.plot(xplot, yplot1)
plt.plot(xplot, yplot2)
