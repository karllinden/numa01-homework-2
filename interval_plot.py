#!/usr/bin/env python3
#
#  interval_plot.py
#
#  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

# One notes that interval multiplication does not distribute.
# For example:
#   ([0,1] - [1,2]) * [1,2] = [-2,0] * [1,2] = [-4,0], and
#   [0,1] * [1,2] - [1,2] * [1,2] = [0,2] - [1,4] = [-4, 1].
# Hence, one should in general expect different results when evaluating
# a polynomial with Horner's scheme and direct computation.
# This is the reason why both methods are plotted.
#
# In the example above the first computation required two operations,
# whereas the second one required three.
# If this is a general pattern, one should expect smaller intervals with
# fewer operations.
# Thus, one expects smaller resulting intervals with Horner's method,
# since it is designed to reduce the number of operations.

import sys

import numpy as np
import matplotlib.pyplot as plt

import interval

def horner(coeff, x):
    """
    Evaluate the polynomial
        c[0] * x^n + c[1] * x^{n-1} + ... + c[n-1] * x + c[n]
    using Horner's method.
    """
    v = 0 # v for value
    for c in coeff:
        v *= x
        v += c
    return v

def direct(coeff, x):
    """
    Evaluate the polynomial
        c[0] * x^n + c[1] * x^{n-1} + ... + c[n-1] * x + c[n]
    using direct computation.
    """
    return sum(c*x**p for (p,c) in enumerate(reversed(coeff)))

# An output filename must be given.
if len(sys.argv) != 2:
    print('invalid number of arguments')
    sys.exit(1)

# Coefficients (from highest to lowest order) for the polynomial to be
# evaluated.
coeff = [3, -2, 5, -1]

# To save memory the xu list given in the instruction is not computed
# explicitly, since it would just be a translated duplicate.
# Instead the +0.5 is added in the for loop below.
xl = np.linspace(0., 1, 1000)

# List of y-values.
yl_direct = []
yu_direct = []
yl_horner = []
yu_horner = []

# One notes that anyhow a for loop is needed, because there is no way
# for numpy to vectorize operations on the custom-made class.
# As a memory optimization the intervals are not stored in a list, but
# generated from each x in xl.
# This way the interpreter can free up the memory needed for the
# instances after each run.
for x in xl:
    i = interval.Interval(x, x + 0.5)

    y_direct = direct(coeff, i)
    yl_direct.append(y_direct.a)
    yu_direct.append(y_direct.b)

    y_horner = horner(coeff, i)
    yl_horner.append(y_horner.a)
    yu_horner.append(y_horner.b)

plt.rc('text', usetex=True)
plt.rc('font', family='serif')

plt.title(r'Plot of $y_l$ and $y_u$ versus $x_l$')
plt.xlabel(r'$x_l$')
plt.ylabel(r'$y$')

plt.plot(xl, yl_direct, label="$y_l$ (direct computation)")
plt.plot(xl, yu_direct, label="$y_u$ (direct computation)")
plt.plot(xl, yl_horner, label="$y_l$ (Horner's method)")
plt.plot(xl, yu_horner, label="$y_u$ (Horner's method)")
plt.grid(True)
plt.legend(loc='upper left')

plt.savefig(sys.argv[1])
