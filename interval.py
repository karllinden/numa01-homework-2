#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#
#  interval.py
#
#  Copyright 2015 Karl Lindén <karl.linden.887@student.lu.se>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#
#

import copy
import numbers
import math

# This class is not written for performance.
# (If one writes software for the performance, one should reconsider
# the choice of programming language...)
# Rather, it is designed to be robust.
# This is the reason why the __init__ method is the only one that
# considers other types than Interval.
# This approach makes it simple to extend the functionality to overload
# more types; just add the code to the __init__ method.
#
# All arithmetic methods in the class are completely blind to other
# types.
# Hence, all methods convert its argument to Interval if it is not
# already given as this type.
# Although it requires more operations (creating a new instance and then
# performing the desired operation), it gives the robustness described
# in the previous paragraph.
#
# Code duplication is kept at a minimum, by implementing the actual
# arithmetics in the incremental (+=, -=, ...) methods, and then
# implementing the copying arithmetic operations (+, -, ...) using
# copy.copy() and wrapping around the incremental operations.
#
# Lastly, the right hand operations are just wrapped around the ordinary
# ones, either directly when the operator commutes (look at __add__ and
# __radd__, for example) or by converting the left hand operand to
# Interval and then performing a regular operation.
# Note that the right-hand operations are only called when the left
# hand operand is not another Interval.
# Thus, it is safe to do an uncoditional Interval conversion in the
# affected routines (__rsub__, __rtruediv__).
class Interval:
    """
    Interval(a,b) returns an interval with a as lower endpoint and b as
    upper endpoint. If b is omitted the degenerate interval
    Interval(a,a) is returned.

    The boundaries must be real values and the lower endpoint must be
    less than or equal to the upper one. Otherwise errors are raised.
    """

    def __init__(self, a, b=None):
        self.a = a
        if b is not None:
            self.b = b
        else:
            self.b = a

        # Sanity checking.
        if not isinstance(self.a, numbers.Real) or \
           not isinstance(self.b, numbers.Real) or \
           not math.isfinite(self.a) or \
           not math.isfinite(self.b):
            raise TypeError('non-real or infinite arguments')
        elif self.a > self.b:
            raise ValueError('bad interval')

    def __repr__(self):
        return '[{}, {}]'.format(self.a, self.b)

    def __contains__(self, x):
        if not isinstance(x, type(self)):
            return Interval(x) in self
        else:
            return self.a <= x.a and x.b <= self.b

    def __iadd__(self, other):
        if not isinstance(other, type(self)):
            self += Interval(other)
        else:
            self.a += other.a
            self.b += other.b
        return self

    def __add__(self, other):
        if not isinstance(other, type(self)):
            # Since interval addition commute it is possible to avoid
            # creating an intermediate instance by converting other to
            # an interval and then performing the addition in place,
            # rather than copying self and then performing an in place
            # addition which would convert other to an intermediate
            # instance.
            result = Interval(other)
            result += self
            return result
        else:
            result = copy.copy(self)
            result += other
            return result
    __radd__ = __add__

    def __isub__(self, other):
        if not isinstance(other, type(self)):
            self -= Interval(other)
        else:
            self.a -= other.b
            self.b -= other.a
        return self

    def __sub__(self, other):
        result = copy.copy(self)
        result -= other
        return result

    def __rsub__(self, other):
        return Interval(other) - self

    def __imul__(self, other):
        if not isinstance(other, type(self)):
            self *= Interval(other)
        else:
            l = [
                self.a * other.a,
                self.a * other.b,
                self.b * other.a,
                self.b * other.b
            ]
            l.sort()
            self.a = l[0]
            self.b = l[-1]
        return self

    def __mul__(self, other):
        if not isinstance(other, type(self)):
            # See corresponding comment in __add__.
            result = Interval(other)
            result *= self
            return result
        else:
            result = copy.copy(self)
            result *= other
            return result
    __rmul__ = __mul__

    def __itruediv__(self, other):
        if not isinstance(other, type(self)):
            self /= Interval(other)
        else:
            if 0 in other:
                raise ZeroDivisionError('division by zero')

            l = [
                self.a / other.a,
                self.a / other.b,
                self.b / other.a,
                self.b / other.b
            ]

            if float('inf') in l or float('-inf') in l:
                raise ValueError('infinite resulting interval')

            l.sort()
            self.a = l[0]
            self.b = l[-1]
        return self

    def __truediv__(self, other):
        result = copy.copy(self)
        result /= other
        return result

    def __rtruediv__(self, other):
        return Interval(other) / self

    def __ipow__(self, n):
        if not isinstance(n, int) or n < 0:
            raise NotImplementedError('interval powers only defined ' +
                                      'for non-negative integers')
        else:
            if n == 0:
                # Define [a,b]^0 to [1,1] (the identity interval over
                # interval multiplication).
                self.a = 1
                self.b = 1
            elif n % 2 == 1 or self.a >= 0:
                self.a **= n
                self.b **= n
            elif self.b < 0:
                self.a, self.b = self.b**n, self.a**n
            else:
                a = self.a ** n
                b = self.b ** n
                self.a = 0
                self.b = max(a, b)
        return self

    def __pow__(self, n):
        result = copy.copy(self)
        result **= n
        return result
